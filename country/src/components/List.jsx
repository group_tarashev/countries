import React, { useContext } from "react";
import { ContextApi } from "../context/ContextProvide";
import Card from "./Card";
import "../styles/list.css";
const List = () => {
  const { data, isLoading, filtered } = useContext(ContextApi);
  if (isLoading) {
    return (
      <div className="loading">
        <h2>Loading ...</h2>
      </div>
    );
  }
  if (filtered !== null) {
    return (
      <div className="list-outer">
        <div className="list">
          {filtered?.map((country, i) => (
            <Card key={country.alpha3Code} country={country} />
          ))}
        </div>
      </div>
    );
  }
  return (
    <div className="list-outer">
      <div className="list">
        {data?.map((country, i) => (
          <Card key={country.alpha3Code} country={country} />
        ))}
      </div>
    </div>
  );
};

export default List;
