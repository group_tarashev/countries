import React, { useContext } from "react";
import { ContextApi } from "../context/ContextProvide";
import "../styles/main.css";
import { MdOutlineSearch } from "react-icons/md";
import List from "./List";
const options = ["Filter by region","Africa", "Americas", "Asia", "Europe", "Oceania"];
const Main = () => {
  const { isLoading, theme, handleChange, handleSearch, search } = useContext(ContextApi);
  
  return (
    <div className={theme ? "main dark-main" : "main light-main"}>
      <div className={theme ? "search-bar " : "search-bar "}>
        <div className="search">
          <div className="srch-icon">
            <MdOutlineSearch color={theme ? "dark" : "light"} />
          </div>
          <input
            className={theme ? "input-search dark" : "input-search ligth"}
            type="text"
            placeholder="Search for a country"
            onChange={handleSearch}
            value={search}
          />
        </div>
        <div className="select-reg">
          <select name="" id="" onChange={handleChange} className={theme ? "select dark" : 'select ligth'}>
            {options.map((item, i) => (
              <option key={i} value={item.toLowerCase()}>{item}</option>
            ))}
          </select>
        </div>
      </div>
      <List/>
    </div>
  );
};

export default Main;
