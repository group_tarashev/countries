import React, { useContext } from 'react'
import { ContextApi } from '../context/ContextProvide'
import '../styles/nav.css'
import { IoMdMoon,IoIosSunny } from "react-icons/io";
const Nav = () => {
    const {theme, setTheme} = useContext(ContextApi)
  return (
    <div className={theme? 'nav dark': 'nav ligth'}>
        <h2>Where in th world?</h2>
        <button className={theme ? "switch-theme dark" : "switch-theme ligth"}onClick={() => setTheme(!theme)}>
            {theme ? <IoIosSunny size={20}/> : <IoMdMoon size={20}/>}
            <span  >{theme ? 'light' :"dark"}</span></button>
    </div>
  )
}

export default Nav