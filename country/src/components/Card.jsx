import React, { useContext } from 'react'
import '../styles/card.css'
import { ContextApi } from '../context/ContextProvide'
import {Link, useNavigate} from 'react-router-dom'
const Card = ({country}) => {
  const navigate = useNavigate();
  const {theme} = useContext(ContextApi)
  return (
    <Link to={`/view/${country.name}`}state={{data:country}} className='link-card'>
    <div className={theme ? 'card dark' : 'card ligth'} >
      <img className='flag' src={country.flag} alt="" />
      <div className="content">
        <h3>{country.name}</h3>
        <ul className='card-content'>
          <li>Population: <span>{country.population}</span></li>
          <li>Region: <span>{country.region}</span></li>
          <li>Capital: <span>{country.capital}</span></li>
        </ul>
      </div>
    </div>
    </Link>
  )
}

export default Card