import React, { useContext } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { ContextApi } from "../context/ContextProvide";
import "../styles/view.css";
const View = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const data = location.state?.data;
  const { theme } = useContext(ContextApi);
  console.log(data);
  return (
    <div className={theme ? `main dark` : "main light"}>
      <div className="view">
        <button className={theme ? "btn-back dark" : "btn-back ligth"}
        onClick={() => navigate('/')} >
          Go back
        </button>
        <div className="view-content">
          <div className="image">
            <img src={data.flag} alt="" />
          </div>
          <div className="country-spec">
            <h2>{data.name}</h2>
            <div className="list-spec">
              <ul className="list-spec-1">
                <li>
                  <span>Native Name:</span> {data.nativeName}
                </li>
                <li>
                  <span>Population:</span> {data.population}
                </li>
                <li>
                  <span>Region:</span> {data.region}
                </li>
                <li>
                  <span>Sub Region:</span> {data.subregion}
                </li>
                <li>
                  <span>Capital:</span> {data.capital}
                </li>
              </ul>
              <ul className="list-spec-2">
                <li>
                  <span>Top Level Domain:</span> {data.topLevelDomain.map((el, i) =>(
                    <article key={i}>{el}</article>
                  ))}
                </li>
                <li>
                  <span>Currency:</span> {data.currencies.map((item, i) =>(
                    <article key={i}>{item.name}</article>
                  ))}
                </li>
                <li>
                  <span>Languages:</span> {data.languages?.map((el, i) => (
                    <article key={i}>{el.name    }</article>
                  ))}
                </li>
              </ul>
            </div>
            { data.borders &&<div className="border-country">
                <span>Border Countries: </span>
                <ul className="brd-cntr">{ data?.borders.map((item, i) =>(
                  <li>{item}</li>
                ))}</ul>
            </div>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default View;
