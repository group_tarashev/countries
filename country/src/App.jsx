import React, { useContext } from "react";
import "./App.css";
import ContextApiProvider, { ContextApi } from "./context/ContextProvide";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Main from "./components/Main";
import View from "./components/View";
import Nav from "./components/Nav";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Main />,
  },
  {
    path: "/view/:id",
    element: <View />,
  },
]);
const App = () => {
  const { theme } = useContext(ContextApi);
  return (
      <div className={theme ? "App dark-main" : "App light-main"}>
        <Nav />
        <RouterProvider router={router} />
      </div>
  );
};

export default App;
