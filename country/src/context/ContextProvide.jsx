import { createContext, useEffect, useState } from "react";
import axios from 'axios'
export const ContextApi = createContext()

const requeset = () =>{
    return axios.get('../../src/assets/data.json')
}

const ContextApiProvider = ({children}) =>{
    const [theme, setTheme] = useState(false);
    const [data, setData] = useState([]);
    const [isLoading, setIsLoading] = useState(true)
    const [filtered, setFiltered] = useState(null);
    const [search, setSearch] = useState(null)
    useEffect(()=>{
        requeset().then(res => {
            setData(res.data)
            setIsLoading(false)
            console.log(res.data);
        })
    },[])
    const handleChange = (e) =>{
        setFiltered(data.filter(item => item.region.toLowerCase() == e.target.value)
        )
        if(e.target.value === 'filter by region'){
            setFiltered(data)
        }
    }
    const handleSearch =(e) =>{
        setSearch(e.target.value.toLowerCase())
        // console.log(e.target.value);
        let found = data.filter(el => el.name.toLowerCase().includes(search))
        setFiltered(found)
        if(e.target.value.length === 0 ){
            setFiltered(null)
        }
    }
    return <ContextApi.Provider value={{data, isLoading, theme, handleChange, setTheme, filtered, handleSearch, search}}>{children}</ContextApi.Provider>
}


export default ContextApiProvider